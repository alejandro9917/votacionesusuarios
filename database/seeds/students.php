<?php

use Illuminate\Database\Seeder;

class students extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Seeder de los estudiantes

        //#01
        DB::table('students')->insert
        ([
            'cod_carnet' => 'AV14024',
            'str_nombre' => 'Sara Elizabeth',
            'str_apellido' => 'Alvarenga Vides',
            'password' => bcrypt('av14024')
        ]);

        //#02
        DB::table('students')->insert
        ([
            'cod_carnet' => 'CP06047',
            'str_nombre' => 'Daniel Wilfredo',
            'str_apellido' => 'Caballero Pimentel',
            'password' => bcrypt('cp06047')
        ]);

        //#03
        DB::table('students')->insert
        ([
            'cod_carnet' => 'CR10063',
            'str_nombre' => 'Katherine Yadira',
            'str_apellido' => 'Canjura Rivas',
            'password' => bcrypt('cr10063')
        ]);

        //#04
        DB::table('students')->insert
        ([
            'cod_carnet' => 'CL16051',
            'str_nombre' => 'Hugo Alexander',
            'str_apellido' => 'Cerón Llanes',
            'password' => bcrypt('cl16051')
        ]);

        //#05
        DB::table('students')->insert
        ([
            'cod_carnet' => 'CO16001',
            'str_nombre' => 'Clara Mercedes',
            'str_apellido' => 'Cuellar Orellana',
            'password' => bcrypt('co16001')
        ]);

        //#06
        DB::table('students')->insert
        ([
            'cod_carnet' => 'CL140259',
            'str_nombre' => 'Jonathan Gamaliel',
            'str_apellido' => 'Cruz Lopez',
            'password' => bcrypt('cl140259')
        ]);

        //#07
        DB::table('students')->insert
        ([
            'cod_carnet' => 'DV15003',
            'str_nombre' => 'Jose Dimas',
            'str_apellido' => 'De la O Villalta',
            'password' => bcrypt('DV15003')
        ]);

        //#08
        DB::table('students')->insert
        ([
            'cod_carnet' => 'DB15002',
            'str_nombre' => 'Diaz Burgos',
            'str_apellido' => 'Milena Iveth',
            'password' => bcrypt('DB15002')
        ]);

        //#09
        DB::table('students')->insert
        ([
            'cod_carnet' => 'EA15001',
            'str_nombre' => 'Azalia Mayeni',
            'str_apellido' => 'Escobar Avalos',
            'password' => bcrypt('ea15001')
        ]);

        //#10
        /*DB::table('students')->insert
        ([
            'cod_carnet' => 'CP06047',
            'str_nombre' => 'Rafel Antonio ',
            'str_apellido' => 'Escobar Rosales',
            'password' => bcrypt('cp06047')
        ]);*/

        //#11
        DB::table('students')->insert
        ([
            'cod_carnet' => 'FR16023',
            'str_nombre' => 'Romeo Emmanuel',
            'str_apellido' => 'Flores Recinos',
            'password' => bcrypt('fr16023')
        ]);

        //#12
        DB::table('students')->insert
        ([
            'cod_carnet' => 'FR14024',
            'str_nombre' => 'Carla Rebeca',
            'str_apellido' => 'Flores Rodriguez',
            'password' => bcrypt('fr14024')
        ]);

        //#13
        DB::table('students')->insert
        ([
            'cod_carnet' => 'FM09026',
            'str_nombre' => 'Angel Leonidas',
            'str_apellido' => 'Flores Mendez',
            'password' => bcrypt('fm09026')
        ]);

        //#14
        DB::table('students')->insert
        ([
            'cod_carnet' => 'GR05088',
            'str_nombre' => 'Brenda Beatriz',
            'str_apellido' => 'Gomez Rodas',
            'password' => bcrypt('gr05088')
        ]);

        //#15
        DB::table('students')->insert
        ([
            'cod_carnet' => 'GI15001',
            'str_nombre' => 'Christian Francisco',
            'str_apellido' => 'Guerra Iglesias',
            'password' => bcrypt('gi15001')
        ]);

        //#16
        DB::table('students')->insert
        ([
            'cod_carnet' => 'GV11028',
            'str_nombre' => 'Jorge Rolando',
            'str_apellido' => 'Guzman Vasquez',
            'password' => bcrypt('gv11028')
        ]);

        //#17
        DB::table('students')->insert
        ([
            'cod_carnet' => 'HC15044',
            'str_nombre' => 'Melvin Leonel',
            'str_apellido' => 'Hernandez Cruz ',
            'password' => bcrypt('hc15044')
        ]);

        //#18
        DB::table('students')->insert
        ([
            'cod_carnet' => 'HC14022',
            'str_nombre' => 'Daniela Adriana',
            'str_apellido' => 'Hernandez Cortez',
            'password' => bcrypt('hc14022')
        ]);

        //#19
        DB::table('students')->insert
        ([
            'cod_carnet' => 'LC150024',
            'str_nombre' => 'Estehffanie Yolanda',
            'str_apellido' => 'Lopez Corado',
            'password' => bcrypt('lc150024')
        ]);

        //#20
        DB::table('students')->insert
        ([
            'cod_carnet' => 'LM14041',
            'str_nombre' => 'Flor Claribel',
            'str_apellido' => 'Lovato Mejia',
            'password' => bcrypt('lm14041')
        ]);

        //#21
        DB::table('students')->insert
        ([
            'cod_carnet' => 'MV14013',
            'str_nombre' => 'Tatiana Vanessa',
            'str_apellido' => 'Mancia Valdivieso',
            'password' => bcrypt('mv14013')
        ]);

        //#22
        DB::table('students')->insert
        ([
            'cod_carnet' => 'MC16019',
            'str_nombre' => 'Francisco Javier',
            'str_apellido' => 'Majano Cortez',
            'password' => bcrypt('mc16019')
        ]);

        //#23
        DB::table('students')->insert
        ([
            'cod_carnet' => 'MM08252',
            'str_nombre' => 'Sindy Estefany',
            'str_apellido' => 'Martinez',
            'password' => bcrypt('mm08252')
        ]);

        //#24
        DB::table('students')->insert
        ([
            'cod_carnet' => 'MC16060',
            'str_nombre' => 'Josselyn Raquel',
            'str_apellido' => 'Martinez Cuellar',
            'password' => bcrypt('mc16060')
        ]);

        //#25
        DB::table('students')->insert
        ([
            'cod_carnet' => 'MV00002',
            'str_nombre' => 'Jazmin Isela',
            'str_apellido' => 'Martinez Valladares',
            'password' => bcrypt('mv00002')
        ]);

        //#26
        DB::table('students')->insert
        ([
            'cod_carnet' => 'MO10047',
            'str_nombre' => 'Rosa Cristina ',
            'str_apellido' => 'Martinez Oliva',
            'password' => bcrypt('mo10047')
        ]);

        //#27
        DB::table('students')->insert
        ([
            'cod_carnet' => 'MM14208',
            'str_nombre' => 'Gladys Nohemy',
            'str_apellido' => 'Mauricio Mejia ',
            'password' => bcrypt('mm14208')
        ]);

        //#28
        DB::table('students')->insert
        ([
            'cod_carnet' => 'AM16066',
            'str_nombre' => 'Ronald Misael',
            'str_apellido' => 'Mixco Aldana',
            'password' => bcrypt('am16066')
        ]);

        //#29
        DB::table('students')->insert
        ([
            'cod_carnet' => 'MM88200',
            'str_nombre' => 'Aura Ivette',
            'str_apellido' => 'Morales',
            'password' => bcrypt('mm88200')
        ]);

        //#30
        DB::table('students')->insert
        ([
            'cod_carnet' => 'NP15006',
            'str_nombre' => 'Juan Carlos',
            'str_apellido' => 'Navarro Portillo',
            'password' => bcrypt('np15006')
        ]);

        //#31
        DB::table('students')->insert
        ([
            'cod_carnet' => 'OH14008',
            'str_nombre' => 'Krissia Stephanie',
            'str_apellido' => 'Orellana Hernniquez',
            'password' => bcrypt('oh14008')
        ]);

        //#32
        DB::table('students')->insert
        ([
            'cod_carnet' => 'PA11015',
            'str_nombre' => 'Diana Margarita',
            'str_apellido' => 'Perez Arabia',
            'password' => bcrypt('pa11015')
        ]);

        //#33
        DB::table('students')->insert
        ([
            'cod_carnet' => 'PV03006',
            'str_nombre' => 'Claudia Yamileth',
            'str_apellido' => 'Perez de Fuentes',
            'password' => bcrypt('pv03006')
        ]);

        //#34
        DB::table('students')->insert
        ([
            'cod_carnet' => 'PM16031',
            'str_nombre' => 'Rudy Aristides',
            'str_apellido' => 'Perez Marquez',
            'password' => bcrypt('pm16031')
        ]);

        //#35
        DB::table('students')->insert
        ([
            'cod_carnet' => 'PZ16001',
            'str_nombre' => 'Sendy Odily',
            'str_apellido' => 'Pineda Zelaya',
            'password' => bcrypt('pz16001')
        ]);

        //#36
        DB::table('students')->insert
        ([
            'cod_carnet' => 'RR14017',
            'str_nombre' => 'Rafael Antonio ',
            'str_apellido' => 'Reynosa Rosales',
            'password' => bcrypt('rr14017')
        ]);

        //#37
        DB::table('students')->insert
        ([
            'cod_carnet' => 'RM15021',
            'str_nombre' => 'Isis Georgina',
            'str_apellido' => 'Rodriguez Mendez',
            'password' => bcrypt('rm15021')
        ]);

        //#38
        DB::table('students')->insert
        ([
            'cod_carnet' => 'RE14010',
            'str_nombre' => 'Kenia Jazmin',
            'str_apellido' => 'Rodriguez Espinales',
            'password' => bcrypt('re14010')
        ]);

        //#39
        DB::table('students')->insert
        ([
            'cod_carnet' => 'RG09012',
            'str_nombre' => 'Daysi Yessenia',
            'str_apellido' => 'Rosa Garcia',
            'password' => bcrypt('rg09012')
        ]);

        //#40
        DB::table('students')->insert
        ([
            'cod_carnet' => 'RG15010',
            'str_nombre' => 'Kenny Maricela',
            'str_apellido' => 'Rosales Gutierrez',
            'password' => bcrypt('rg15010')
        ]);

        //#41
        DB::table('students')->insert
        ([
            'cod_carnet' => 'SG14042',
            'str_nombre' => 'Nathaly Stefany',
            'str_apellido' => 'Serrano Gonzalez',
            'password' => bcrypt('sg14042')
        ]);

        //#42
        DB::table('students')->insert
        ([
            'cod_carnet' => 'SR15057',
            'str_nombre' => 'Anita Keren',
            'str_apellido' => 'Serrano Rodriguez',
            'password' => bcrypt('sr15057')
        ]);

        //#43
        DB::table('students')->insert
        ([
            'cod_carnet' => 'SC11061',
            'str_nombre' => 'Oscar Manolo',
            'str_apellido' => 'Shunico Castillo',
            'password' => bcrypt('sc11061')
        ]);

        //#44
        DB::table('students')->insert
        ([
            'cod_carnet' => 'FS15006',
            'str_nombre' => 'Manuel',
            'str_apellido' => 'Sibrian Flores',
            'password' => bcrypt('fs15006')
        ]);

        //#45
        DB::table('students')->insert
        ([
            'cod_carnet' => 'SR13070',
            'str_nombre' => 'Manuel de Jesus',
            'str_apellido' => 'Soriano Renderos ',
            'password' => bcrypt('sr13070')
        ]);

        //46
        DB::table('students')->insert
        ([
            'cod_carnet' => 'AH06041',
            'str_nombre' => 'Wilber Alexander',
            'str_apellido' => 'Aquino Hernandez',
            'password' => bcrypt('ah06041')
        ]);

        //46
        DB::table('students')->insert
        ([
            'cod_carnet' => 'GV15004',
            'str_nombre' => 'Katherine Vanessa',
            'str_apellido' => 'Gutierrez Varela',
            'password' => bcrypt('gv15004')
        ]);

        //46
        DB::table('students')->insert
        ([
            'cod_carnet' => 'GR09025',
            'str_nombre' => 'José Mauricio',
            'str_apellido' => 'Garcia Romero',
            'password' => bcrypt('gr09025')
        ]);

        //46
        DB::table('students')->insert
        ([
            'cod_carnet' => 'RV06030',
            'str_nombre' => 'Rocío Marisol',
            'str_apellido' => 'Rameriz Velasco',
            'password' => bcrypt('rv06030')
        ]);

        //46
        DB::table('students')->insert
        ([
            'cod_carnet' => 'Lic1234',
            'str_nombre' => '',
            'str_apellido' => '',
            'password' => bcrypt('lic1234')
        ]);
    }
}
